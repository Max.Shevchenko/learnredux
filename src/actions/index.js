export function moviesList() {
    return {
        type: 'MOVIES_LIST',
        payload: [
            {id: 1, name: 'Pulp Fiction'},
            {id: 2, name: 'Zayec'},
            {id: 3, name: 'Valera'}
        ]
    }
}


export function dirList() {
    return {
        type: 'DIR_LIST',
        payload: [
            {id: 1, name: 'Tarantino'},
            {id: 2, name: 'Scorcese'},
            {id: 3, name: 'Spielberg'},
        ]
    }
}



