import React, {Component} from 'react'
import {connect} from 'react-redux'
import {moviesList, dirList} from './actions'

class App extends Component {

    componentWillMount() {
        this.props.getMovies()
        this.props.getDirectors()
    }

    render() {
        console.log(this.props)
        return (
            <div>
                hello
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.movies
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMovies: () => {
            dispatch(moviesList())
        },
        getDirectors: () => {
            dispatch(dirList())
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(App)